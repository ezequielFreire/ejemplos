/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ejemplos;

/**
 *
 * @author Ezequiel
 */
public class Ejemplos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        //Metodo split para separar cadena de caracteres
        String fullname = "Mario Perez";
        String [] cadena = fullname.split(" ");
        for(String res : cadena){
            System.out.println(res);
        }
    }
    
}

/*
 * Ejemplos de utilizacion de metodos de la clase String
 * 
 * 
 */

package ejemplostring;

/**
 *
 * @author Ezequiel A. Freire
 */
public class EjemploString {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //Metodo split para separar cadena de caracteres
        String fullname = "Mario Perez";
        String [] cadena = fullname.split(" ");
        for(String res : cadena){
            System.out.println(res);
        }
        
        //Uso de charAt(int) para invertir cadena de caracteres
        String cadenaInvertida = "";
        
        for(int i = fullname.length()-1; i >= 0; i--){
            cadenaInvertida = cadenaInvertida + fullname.charAt(i);
        }
        System.out.println(cadenaInvertida);
        
        //Metodo indexOf(String) busqueda de un palabra en una oracion
        String oracion = "No existe una combinación de sucesos que "
                       + "la inteligencia de un hombre no sea capaz de explicar.";
        String palabra = "inteligencia";
        
        if(oracion.indexOf(palabra) != -1 ){
            System.out.println("se encontro la palabra: " + palabra);
        }else{
            System.out.println("no se encontro la palbra: " + palabra );
        }
        
        //Metodo replace(char old, char new)
        String texto = "3.14159265358979";
        System.out.println(texto.replace(".", ","));
        
        //Uso de metodo toUpperCase() y toLowerCase()
        String cadMayuscula = "mayuscula";
        String cadMinuscula = "MINUSCULA";
        
        System.out.println("toUpperCase(): " + cadMayuscula.toUpperCase());
        System.out.println("toLowerCase(): " + cadMinuscula.toLowerCase());
        
        //Uso de metodo substring(int incio, int final)
        String testo = "texto de prueba";
        System.out.println(texto.substring(6, 15));
        
    }
    
}
